# ajax #

v1.2.26

(Documentation is not up to date)

This module enables AJAX requests through POST & GET and also File upload.
It also enables conversion from URL to JSON and from JSON to URL.

## Getting Started ##

* Make sure you have this in you HTML page.

```html
<div id="cacheBlock">
    <div id="ajaxLoading">
        <img src="images/loaderImage.gif" alt="loaderImage" />
    </div>
</div>
```

* Setup the GameSettings file :

```js
// app/script/Settings/GameSettings.js

var dependencies = {};
var list = MadJohRequire.getList(dependencies);
define(list, function(require){
	var GameSettings = {
		id_Game : 3,
		version : '1.0.0',
		name : 'AppName',
		bundle : 'fr.madjoh.appname',

		// Here is the important line :
		apiURL : 'http://madjoh.com/appname'
	};

	return GameSettings;
});
```

## API ##

- Send Requests
	- [get](#get-post-postauth-postfileauth)
	- [post](#get-post-postauth-postfileauth)
	- [postAuth](#get-post-postauth-postfileauth)
	- [postFileAuth](#get-post-postauth-postfileauth)
- Animate Loading
	- [showLoader](#showloader-hideloader)
	- [hideLoader](#showloader-hideloader)
- Encode / Decode URL
	- [URLToJSON](#urltojson-jsontourl)
	- [JSONToURL](#urltojson-jsontourl)



## get, post, postAuth, postFileAuth ##

```js
AJAX.get 		(target,[callback, [parameters, [loader, [async]]]]);
AJAX.post 		(route, [callback, [parameters, [loader, [async]]]]);
AJAX.postAuth 	(route, [callback, [parameters, [loader, [async]]]]);
AJAX.postFileAuth(route, callback, file, [parameters, [filename, [loader, [progressCallback]]]]]]);
```

| Parameter 		| Type 		| Default  	| Description 																			|
| ------------------| :-------: | :--------:| ------------------------------------------------------------------------------------: |
| target*			| string 	| 			| The URL your request is targetting 													|
| route*			| string 	| 			| The route of your target added to the apiURL defined in GameSettings.					|
| callback 			| function	| none 		| A function to execute once the data has come back from the target page on the server 	|
| parameters 		| json		| \{ \} 	| Contains the data to send to the target page as a {key : value} object.				|
| loader 			| boolean 	| false 	| Set to true if you wish to see the loading animation.	 								|
| async 			| boolean  	| true 		| set to true if you wish the request to be asynchronuous.								|
| file 				| file  	| none 		| The file to send to the server														|
| filename 			| string  	| myFile 	| The name of the file to send to the server											|
| progressCallback 	| function  | none 		| A function to execute during the file sending progression. 							|


#### Example ####
```js
var parameters = {
	mail 		: 'xyz@hotmail.com',
	password 	: 'password'
};

var callback = function(result){
	if(result.status === 1){
		console.log('You are connected');
	}else{
		console.log('Oops, an error occured');
	}
};

AJAX.post('/user/login', callback, parameters, true);
```

## URLToJSON, JSONToURL ##

#### Example ####
```js
var url = 'http://madjoh.com/media/show?mail="jonathanhattab@madjoh.com"&token="EROGJ123fazef34ZERGG"';

var json = AJAX.URLToJSON(url);
// json = {mail : 'jonathanhattab@madjoh.com', token : 'EROGJ123fazef34ZERGG'};

var url2 = AJAX.JSONToURL('http://madjoh.com/media/show', json);
// url2 = 'http://madjoh.com/media/show?mail="jonathanhattab@madjoh.com"&token="EROGJ123fazef34ZERGG"';
```