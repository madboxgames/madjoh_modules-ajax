define([
	'require',
	'madjoh_modules/ajax/request',
	'madjoh_modules/session/session',
	'madjoh_modules/msgbox/msgbox',
	'madjoh_modules/language/language',
	'madjoh_modules/custom_events/custom_events'
], 
function (require, Request, Session, MsgBox, Language, CustomEvents){
	var AJAX = {
		//======================//
		// 		PUBLIC API 		//
		//======================//
			get 	: function(route, options){return AJAX.sendRequest(route, options, 'get'  );},
			post 	: function(route, options){return AJAX.sendRequest(route, options, 'post' );},
			file 	: function(route, options){return AJAX.sendRequest(route, options, 'file' );},

		//======================//
		// 		INITIALIZE 		//
		//======================//
			init : function(settings){
				AJAX.settings = settings;

				var deviceId = localStorage.getItem('com.madjoh.deviceId');
				if(!deviceId){
					deviceId = AJAX.getRandomString();
					localStorage.setItem('com.madjoh.deviceId', deviceId); 
				}
				document.deviceId = deviceId;
				document.requests = {};
			},
			
		//======================//
		// 	   SEND REQUEST 	//
		//======================//
			sendRequest : function(route, options, type){
				options = AJAX.reviewOptions(options);
					
				if(!options.noAuth && !Session.isOpen()){
					Session.close(true);
					AJAX.log(route, type, 'You must be logged in');
					return Promise.reject();
				}

				AJAX.addParameters(options);

				if(!route){
					AJAX.log(route, type, 'Missing route. ' + route);
					return Promise.reject();
				}

				var key = type + '_' + route + '_' + JSON.stringify(options.parameters);
				if(document.requests[key]) return document.requests[key];

				var request = new Request(type, route, options, AJAX.settings);
				var promise	= request.send().then(
					function(result){
						document.requests[key] = null;
						return Promise.resolve(result);
					},
					function(data){
						console.log('rejected request');
						document.requests[key] = null;
						var type = (data && data.error && typeof data.error === 'string' && AJAX.errors[data.error]) ? data.error : 'other';
						CustomEvents.fireCustomEvent(document, 'LogEvent', {event : 'Error', type : type, add : 1});
						if(data && data.error && typeof data.error === 'string' && AJAX.errors[data.error]) AJAX.errors[data.error](data.request);
						return Promise.reject(data);
					}
				);
				if(options.fakeProgress) AJAX.settings.fakeProgress(promise);

				document.requests[key] = promise;

				return promise;
			},

		//======================//
		// 	     OPTIONS 		//
		//======================//
			reviewOptions : function(options){
				if(!options) 					options 								= {};
				if(typeof options.parameters 	=== 'undefined') options.parameters 	= {};
				if(typeof options.loader 		=== 'undefined') options.loader 		= false;
				if(typeof options.async 		=== 'undefined') options.async 			= true;
				if(typeof options.file 			=== 'undefined') options.file 			= null;
				if(typeof options.fileName 		=== 'undefined') options.fileName 		= 'filename';
				if(typeof options.ext 			=== 'undefined') options.ext 			= false;
				if(typeof options.sync 			=== 'undefined') options.sync 			= options.loader;
				if(!options.progressCallback && options.file) options.progressCallback 	= AJAX.settings.onProgress;

				return options;
			},
			addParameters : function(options){
				if(!options.noAuth){
					var keys = Session.getKeys();
					options.parameters.id 		= keys.id;
					options.parameters.token 	= keys.token;
				}
					
				options.parameters.deviceId 	= document.deviceId;
				options.parameters.app_version 	= AJAX.settings.version;
				options.parameters.language 	= Language.get();

				if(options.file && !options.parameters.fileType) 	options.parameters.fileType = options.file.type.split('/')[1];
				if(!window.device || !window.device.platform) 		options.parameters.device = 'browser';
				else 												options.parameters.device = device.platform.toLowerCase();
			},

		//======================//
		// 		 TOOLBOX 		//
		//======================//
			log : function(route, type, message){
				console.log('[AJAX][' + type.toUpperCase()+'] ' + message, route);
			},
			getRandomString : function(){
				var characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
				var token = '';
				for (var i = 0; i < 10; i++) token += characters[Math.floor(Math.random() * (characters.length - 1))];
				return token
			},

		//======================//
		// 		 ON ERROR 		//
		//======================//
			errors : {
				SERVER_ERROR 	: function(request){
					if(!request.options.sync) return;
					MsgBox.show('server_error');
				},
				ERROR 			: function(request){
					AJAX.log(request.route, request.type, 'Unknown error status : ' + request.xhr.status + '//' + request.xhr.statusText);
					if(!request.options.sync) return;
					MsgBox.show('error');
				}
			}
	};

	return AJAX;
});