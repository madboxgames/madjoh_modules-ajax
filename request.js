define([
	'require',
	'madjoh_modules/json_url/json_url',
	'madjoh_modules/spinner/spinner',
	'madjoh_modules/msgbox/msgbox',
	'madjoh_modules/custom_events/custom_events',
	'madjoh_modules/api_middleware/api_middleware',
	'madjoh_modules/session/session'
], 
function (require, JSON_URL, Spinner, MsgBox, CustomEvents, APIMiddleware, Session){
	function Request(type, route, options, settings){
		this.type 		= type;
		this.route 		= route;
		this.nbTry 		= 0;
		this.options 	= options;
		this.settings 	= settings;
	}

	// XHR
		Request.prototype.initXHR = function(){
			var request = this;
			this.xhr = new XMLHttpRequest();
			if(this.type === 'get') this.xhr.overrideMimeType('text/plain; charset=x-user-defined');
			this.xhr.onreadystatechange = function(){
				if(request.xhr.readyState !== 4) 	return;
				if(request.aborted) 				return;

				if(request.options.loader) Spinner.hide();
				request.clearTimeout();

				if(request.xhr.status === 200){
					if(request.xhr.responseText){
						CustomEvents.fireCustomEvent(document, 'AJAX_SUCCESS');

						if(request.type === 'get') return request.promiseActions.resolve(request.xhr.responseText);

						var result = JSON.parse(request.xhr.responseText);
						return APIMiddleware.handle(result, request, request.settings.errors);
					}
					else request.onNoInternet();
				}
				else if(request.xhr.status === 0) 	request.onNoInternet();
				else if(request.xhr.status === 500) request.fail('SERVER_ERROR');
				else 								request.fail('ERROR');
			};
		};

	// SEND
		Request.prototype.send = function(){
			var request = this;
			if(!this.promise){
				this.promise = new Promise(function(resolve, reject){
					request.promiseActions = {
						resolve : resolve,
						reject 	: reject
					};
				});
			}

			if(!this.options.noAuth && !Session.isOpen()){
				return this.promiseActions.reject();
			}

			if(this.options.loader) Spinner.show();

			this.aborted = false;

			var sendKey = 'send_' + this.type.toLowerCase();
			this.initXHR();
			if(this.options.progressCallback) this.setupProgressCallback();
			this[sendKey]();
			this.postponeTimeout();

			return this.promise;
		};
		Request.prototype.send_get = function(){
			var parameters = (this.options.skipParameters) ? {} : this.options.parameters;
			var target = JSON_URL.toURL(this.route, parameters);

			this.xhr.open('GET', target, this.options.async);
			this.xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			this.xhr.send(null);
		};
		Request.prototype.send_post = function(){
			var params = JSON_URL.toURL('', this.options.parameters);
			var target = (this.options.ext) ? this.route : this.settings.getAPI() + this.route;

			this.xhr.open('POST', target, this.options.async);
			this.xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
			this.xhr.send(params);
		};
		Request.prototype.send_file = function(){
			// FORM DATA
				var fd = new FormData();
				for(var key in this.options.parameters) fd.append(key, this.options.parameters[key]);
				fd.append(this.options.fileName, this.options.file);

			// SEND
				var target = (this.options.ext) ? this.route : this.settings.getAPI() + this.route;

				this.xhr.open('POST', target, true);
				this.xhr.send(fd);
		};

	// SUCCEED & FAIL
		Request.prototype.succeed = function(result){
			this.promiseActions.resolve(result);
		};
		Request.prototype.fail = function(reason){
			this.log('['+reason.toUpperCase()+'] Aborting request on ' + this.route);
			if(reason.toUpperCase() === 'INTERNET_ERROR'){
				var request = this;
				MsgBox.show('internet_error', {
					onOk 		: function(){request.send();},
					onCancel 	: function(){request.promiseActions.reject({reason : 'INTERNET_ERROR', request : request});}
				});
			}
			else{
				this.promiseActions.reject({reason : reason, request : this});
			}
		};

	// PROGRESS
		Request.prototype.setupProgressCallback = function(){
			var request = this;
			var element = (this.type.toLowerCase() === 'file') ? this.xhr.upload : this.xhr;
			element.addEventListener('progress', function(e){
				if(e.lengthComputable){
					request.postponeTimeout();

					var percentage = Math.round((e.loaded * 100) / e.total);
					request.options.progressCallback(percentage);
				}
			}, false);
			element.addEventListener('load', function(e){request.options.progressCallback(100);}, false);

			this.options.progressCallback(0);
		};

	// INTERNET ERROR
		Request.prototype.onNoInternet = function(){
			if(!this.options.sync) return this.attachToEvent();
			this.onError('INTERNET_ERROR');
		};
		Request.prototype.attachToEvent = function(){
			var request = this;
			var id = CustomEvents.addCustomEventListener(document, 'AJAX_SUCCESS', function(){
				CustomEvents.removeCustomEventListener(document, 'AJAX_SUCCESS', id);
				request.onError('INTERNET_ERROR');
			});
		};

	// TIMEOUT & ABORT
		Request.prototype.timeoutMap = {
			short 		: 500,
			get 		: 10000,
			post 		: 6000,
			file 		: 15000
		};
		Request.prototype.nbTryMap = {
			get 		: 3,
			post 		: 3,
			file 		: 2
		};
		Request.prototype.postponeTimeout = function(){
			this.clearTimeout();
			var request = this;
			this.timeout = setTimeout(function(){
				request.abort();
				request.onError('TIMEOUT');
			}, this.timeoutMap[this.type]);
		};
		Request.prototype.clearTimeout = function(){
			if(!this.timeout) return;

			clearTimeout(this.timeout);
			this.timeout = null;
		};
		Request.prototype.abort = function(){
			this.clearTimeout();
			this.aborted = true;
			this.xhr.abort();
		};
		Request.prototype.onError = function(reason){
			this.nbTry++;
			if(this.nbTry < this.nbTryMap[this.type]){
				this.log('['+reason.toUpperCase()+'](' + this.nbTry + ') on ' + this.route + ' (retrying)');
				var request = this;
				setTimeout(function(){
					console.log('Sending Again after [' + reason.toUpperCase() + ']');
					request.send();
				}, this.timeoutMap.short); 
			}
			else{
				this.log('['+reason.toUpperCase()+'](' + this.nbTry + ') on ' + this.route + ' (aborting)');
				this.fail(reason.toUpperCase());
			}
		};

	// TOOLS
		Request.prototype.log = function(message){
			console.log('[REQUEST]['+this.type.toUpperCase()+']' + message);
		};

	return Request;
});